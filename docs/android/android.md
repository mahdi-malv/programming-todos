# Android native

[RoadMap](https://trello-attachments.s3.amazonaws.com/5c6cf13b112b2d0494c1a480/5d2b0cca834cbf396bc8a7c6/95e001dfe06d1abcb37c818c868111a0/image_2019-07-12_22-38-45.png)

<details>
<summary>Road map image (Click to expand)</summary>

<img src="/android/roadmap.png" />

</details>




## Pattern and Archs

- MVVM
- Testing in MVP
- TDD
- Clean

## Components

### Fragments

> [Link](https://developer.android.com/guide/components/fragments)

- Fragment
- Fragment backstack
- Fragment life cycle
- Multiple fragments

### Change language and theme

- Localization
- Change theme
- Android 8 dark mode

### Font

- Font: Xml, Downloadable

### Storage

- SharedPref
- Storage and FileProvider

### Task scheduling

- Workmanager
- Job scheduler

## Hardware

- Camera with CameraX

## Testing

- Junit -- Mockk
- Espresso and Robolectric

## Build tools

- Advanced tasking with Gradle
- Proguard
- App bundle test

## UI (low priority)

- Animations
- Jetpack compose

## Firebase (low priority)

- Messaging
- Analytics
- Realtime database
- Crash
- Firestore

- Maps
