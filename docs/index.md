# Programming TODOs

> Version 0.0.1-alpha6

## Frameworks

### [Android native](android_native.md)
> [Test project](https://github.com/mahdi-malv/Test)
> 
> [Test project SSH](git@github.com:mahdi-malv/test.git)

### Flutter
> NO test


## Languages

### [Kotlin](kotlin.md)
> [Test project](https://gitlab.com/mahdi-malv/kotlin-test)
> 
> [Test project SSH](git@gitlab.com:mahdi-malv/kotlin-test.git)

### Dart

> [Test project](https://gitlab.com/mahdi-malv/flutter-test)
> 
> [Test project SSH](git@gitlab.com:mahdi-malv/flutter-test.git)

### [Python](python/python.md)

### JS

##  Tools

### [Gitlab-CI](ci.md)

### Travis

### Docker

## Theory

### Linear Algebra

### AI stuff
